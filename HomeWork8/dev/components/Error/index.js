import React, {Component} from 'react';

class Error extends Component {

    componentDidMount() {
		setTimeout(() => {
			this.props.history.push('/');			
		}, 2000);
	}

    render() {
		
		return (
			<>
				<p>News not found</p>
			</>
		);
	}
}

export default Error;