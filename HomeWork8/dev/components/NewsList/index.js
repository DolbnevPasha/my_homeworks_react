import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import styles from './style.css';

class NewsList extends Component {
	render() {
		const { listNews } = this.props;
		return (
			<ul className={styles.newsList}>
				{
					listNews.map((item) => {
						return <li 
							key={item.id}
							className={styles.linkNews}
						>
							<Link
								to={`/${item.id}`}
							>
								{item.title}
							</Link>
						</li>
					})
				}
			</ul>
		);
	}
}

export default NewsList;

//<Item
//	key={item.id}
//	item={item}
///>