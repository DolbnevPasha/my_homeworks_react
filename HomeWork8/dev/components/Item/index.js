import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Button from '../Button';
import Icon from '../Icon';
import Form from '../Form';

import styles from './style.css';

class Item extends Component {
    constructor(props) {
        super();

        this.state = {
            isShowForm: false,
            item: props.news.find(item => item.id == props.match.params.newsId) || null,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news.find(item => item.id == nextProps.match.params.newsId)) {
            this.setState({
                item: nextProps.news.find(item => item.id == nextProps.match.params.newsId)
            });
        } else {
            nextProps.history.push('/error');
        }
    }

    handleChange() {
        const { isShowForm } = this.state;
        this.setState({
            isShowForm: !isShowForm,
        });
    }

    render() {
        const { isShowForm, item } = this.state;
        
        return (

            <li>
                {
                    isShowForm ? (
                        <Form
                            type="update"
                            item={item}
                            hideForm={this.handleChange}
                            contentForButton="Редактировать"
                        />
                    ) : null
                }
                {
                    item ? (
                    <article className={styles.news}>
                        <header>
                            <h1>{item.title}</h1>
                            <div>
                                <Button
                                    type="editNews"
                                    theme="small"
                                    handleClick={this.handleChange}
                                >
                                    <Icon name="edit" />
                                </Button>
                                <Button
                                    type="deleteNews"
                                    theme="small"
                                    itemId={item.id}
                                >
                                    <Icon name="delete" />
                                </Button>
                            </div>
                        </header>
                        <p>{item.body}</p>
                    </article>
                    ) : (
                            <p>Loading</p>
                        )
                }
            </li>
        );
    }
}

export default withRouter(connect(store => ({ news: store }))(Item));