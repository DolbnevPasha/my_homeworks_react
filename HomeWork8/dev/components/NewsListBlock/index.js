import React, { Component } from 'react';
import { connect } from 'react-redux';	
import {
	BrowserRouter as Router,
	Route,
  NavLink,
	Switch
} from "react-router-dom";

import NewsList from '../NewsList';
import Button from '../Button';
import BlockAddNews from '../BlockAddNews';
import Item from '../Item';
import Error from '../Error';

import { setNews } from '../../actions';

import { getNews } from '../../api';

class NewsListBlock extends Component {
	constructor(){
		super();

		this.state = {
			amountNews: 2
		};

		this.moreNews = this.moreNews.bind(this);
		this.arrayNewsForShow = this.arrayNewsForShow.bind(this);
	}

	componentWillMount(){
		getNews()
			.then((response) => {
				this.props.setNews(response.data);
			})
	}

	moreNews(){
		const { amountNews } = this.state;
		const { news } = this.props;

		if(amountNews < news.length){
			this.setState({
				amountNews: amountNews+2,
			});
		}
	}
    
    arrayNewsForShow(){
		const { amountNews } = this.state;
		const { news } = this.props;
		return news.filter((item, index) => {
					if(index < amountNews){
						return item;
					}
		        })
	}

	render() {	
		const { amountNews } = this.state;
		const { news } = this.props;
		return (
			<Router>
				<Switch>
					<Route path="/error" component={Error} />
					<Route path="/:newsId" component={Item} />
					<Route path="/" exact>
						<BlockAddNews/>
						<NewsList
							listNews={this.arrayNewsForShow()}
            />
						<Button
							type="moreNews"
							theme="more"
							handleClick={this.moreNews}
						>
							{amountNews < news.length ? "Показать еще" : "Новостей больше нет"}
						</Button>
					</Route>
				</Switch>
      </Router>
		);
	}														
}

const mapStateToProps = store => ({ news: store });

export default connect(
	mapStateToProps, { setNews }
)(NewsListBlock);