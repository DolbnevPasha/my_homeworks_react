import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { removeNews } from '../../actions';

import styles from './style.css';

class Button extends Component {
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.getClassName = this.getClassName.bind(this);
    }

    handleChange(){
        const {type, handleClick, itemId, removeNews, history} = this.props;
        switch (type) {
            case 'moreNews':
            case 'addNews':             
            case 'editNews':
                handleClick();
                break;                
            case 'deleteNews':
                history.push('/');
                removeNews(itemId);
                break;  
            case 'buttonForm':   
                break;               
            default:
                break;
        }
    }

    getClassName(){
        const { theme } = this.props;
        switch (theme) {
            case 'more':
                return styles.more;
            case 'add':
                return styles.add;               
            case 'post':
                return styles.post;                
            case 'small':
                return styles.small;                
            default:
                break;
        }
    }

    render() {

        const { children } = this.props;
        return (
            <button
                className={this.getClassName()}
                onClick={this.handleChange}
            >
                {children}
            </button>
        );
    }
}

//const mapDispatchToProps = (dispatch) => ({
//    removeNews: (id) => dispatch(removeNews(id))
//});

//export default connect(null, mapDispatchToProps)(Button);

export default withRouter(connect(null, { removeNews })(Button));