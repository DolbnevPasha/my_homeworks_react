const path = require('path');

//const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const env = process.env.NODE_ENV || 'development';

module.exports = {
	entry: './dev/script.js',
	output: {
		path: path.resolve(__dirname, 'site'),
		filename: 'bundle.[chunkhash].js'
	},
	mode: env,
	devServer: {                                              
		contentBase: path.resolve(__dirname, 'site'),
		host: 'localhost',
		port: 9090,
		historyApiFallback: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
				}
			},
			{
        		test: /\.(svg)$/,
        		exclude: /node_modules/,
        		use: [
        			'babel-loader',
        			'svg-react-loader'
        		]
        	},
			{
                test: /\.css$/,
				use: [
                    MiniCSSExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[local]--[hash:base64:5]',
                            }
                        }
                    }
                ]
            },
		]
	},

	plugins: [
        new CleanWebpackPlugin(),
        new WebpackMd5Hash(),
        new MiniCSSExtractPlugin({					
            filename: 'style.[chunkhash].css'
        }),
		// new CopyWebpackPlugin([
		// 	{
		// 		from: path.resolve('./dev/static'),
		// 		to: path.resolve('./site')
		// 	}	
		// ]),
        new HtmlWebpackPlugin({
            inject: false,
            template: './dev/template/index.html',
            filename: 'index.html'
        })
	]
}