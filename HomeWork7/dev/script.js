import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';	

import NewsListBlock from './components/NewsListBlock';

import store from './store';

import styles from './style.css';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<div className={styles.box}>
					<NewsListBlock />
				</div>
			</Provider>
		);
	}
}

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);