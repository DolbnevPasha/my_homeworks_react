const defaultStore = [];

const reducer = (store=defaultStore, { type, payload }) => {
	let news = store;
	switch(type) {
		case 'CREATE_NEWS': {
			return [payload, ...store];
		}
		case 'UPDATE_NEWS': {
			return news.map(item => item.id === payload.id ? payload : item);
		}
		case 'REMOVE_NEWS': {
			return news.filter(item => item.id !== payload);
		}
		case 'SET_NEWS': {
			return payload;
		}
		default: {
			return news;
		}
	}
};

export default reducer;