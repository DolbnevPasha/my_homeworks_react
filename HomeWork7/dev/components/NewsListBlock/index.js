import React, { Component } from 'react';
import { connect } from 'react-redux';	

import NewsList from '../NewsList';
import Button from '../Button';
import BlockAddNews from '../BlockAddNews';

import { setNews } from '../../actions';

import { getNews } from '../../api';

class NewsListBlock extends Component {
	constructor(){
		super();

		this.state = {
			amountNews: 2
		};

		this.moreNews = this.moreNews.bind(this);
        this.arrayNewsForShow = this.arrayNewsForShow.bind(this);
	}

	componentWillMount(){
		getNews()
			.then((response) => {
				this.props.setNews(response.data);
			})
	}

	moreNews(){
		const { amountNews } = this.state;
		const { news } = this.props;

		if(amountNews < news.length){
			this.setState({
				amountNews: amountNews+2,
			});
		}
	}
    
    arrayNewsForShow(){
		const { amountNews } = this.state;
		const { news } = this.props;
		return news.filter((item, index) => {
					if(index < amountNews){
						return item;
					}
		        })
	}

	render() {	
		const { amountNews } = this.state;
		const { news } = this.props;															

		return (
			<>
				<BlockAddNews/>
				<NewsList
					listNews={this.arrayNewsForShow()}
                />
				<Button
					type="moreNews"
					theme="more"
					handleClick={this.moreNews}
				>
					{amountNews < news.length ? "Показать еще" : "Новостей больше нет"}
				</Button>
            </>
		);
	}														
}

const mapStateToProps = store => ({ news: store });

export default connect(
	mapStateToProps, { setNews }
)(NewsListBlock);