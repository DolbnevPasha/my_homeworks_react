import React, { Component } from 'react';
import { connect } from 'react-redux';

import { updateNews, createNews } from '../../actions';

import Button from '../Button';

import styles from './style.css';

class Form extends Component {
    constructor(props){
        super();

        this.state = {
            title: props.item ? props.item.title : '',
            text: props.item ? props.item.body : '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clearForm = this.clearForm.bind(this);
    }

    handleChange(e) {                          
        const { name, value } = e.target;      

        this.setState({                        
            [name]: value,                     
        });
    }
    
    handleSubmit(e) {
        e.preventDefault();

        const { title, text } = this.state;
        const { type, item, hideForm, updateNews, createNews} = this.props;

        const data = {                           
            id: item ? item.id : Date.now(),     
            title: title,                        
            body: text,                          
        };

        switch (type) {
            case 'create':
                createNews(data);
                break;
            case 'update':
                updateNews(data);
                break;
            default:
                break;
        }
        
        hideForm();
        this.clearForm();
    }

    clearForm() {                          
        this.setState({
            title: '',
            text: '',
        });
    }

    render() {
        const { title, text } = this.state;
        
        return (
            <div
                className={styles.wrap}
            >
                <form
                    className={styles.form}
                    onSubmit={this.handleSubmit}
                >
                    <label htmlFor="title">Заголовок</label>
                    <input
                        type="text"
                        id="title"
                        name="title"
                        value={title}
                        onChange={this.handleChange}
                    />

                    <label htmlFor="text">Текст</label>
                    <textarea
                        name="text"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                    />
                    <Button
                        type="buttonForm"
                        theme="post"
                    >
                        {this.props.contentForButton}
                    </Button>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    createNews: (news) => dispatch(createNews(news)),
    updateNews: (news) => dispatch(updateNews(news))
});

export default connect(null, mapDispatchToProps)(Form);

// export default connect(null, { createNews, updateNews })(Form);