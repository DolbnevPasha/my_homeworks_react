import React from 'react';
import ReactDOM from 'react-dom';

import NewsListBlock from './components/NewsListBlock'

import styles from './style.css';

const App = () => (	
	<div className={styles.box}>
		<NewsListBlock />
	</div>
);

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);