import React from 'react';

import styles from './style.css';

const Button = (props) => {

    let cls = '';
    switch (props.theme) {
		case 'more': 
            cls = styles.more;
            break;
		case 'add':
			cls = styles.add;
            break;
        case 'post':
			cls = styles.post;
            break;
        case 'small':
            cls = styles.small;
            break;
		default:
			break;
	}

    return (
        <button
            className={cls}
            onClick={props.handleClick}
        >
            {props.children}
        </button>
    );
} 

export default Button;