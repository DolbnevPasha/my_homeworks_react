import React, { useState } from 'react';

import Button from '../Button'
import Form from '../Form'

import styles from './style.css';

const BlockAddNews = ({addNews}) => {
    const [isShowForm, handleShowForm] = useState(false);
        
    const showHideForm = () => {
		handleShowForm(!isShowForm);
    }

    return (
        <div
            className={styles.block}
        >
            <Button
                theme="add"
                handleClick={showHideForm}
            >
                Добавить новость
            </Button>
            {
                isShowForm ? (
                    <Form
                        addNews={addNews}
                        hideForm={showHideForm}
                        contentForButton="Опубликовать"
                    />
                ) : null
            }
        </div>
    );   
}

export default BlockAddNews;