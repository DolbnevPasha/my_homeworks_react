import React, { useState, useEffect } from 'react';

import NewsList from '../NewsList'
import Button from '../Button'
import BlockAddNews from '../BlockAddNews'

const NewsListBlock = () => {
	const [amountNews, handleAmountNews] = useState(2);
	const [listItem, changeListItem] = useState(JSON.parse(localStorage.getItem('Новости')) || []);
	
	const moreNews = () => {
		if(amountNews < listItem.length){
			handleAmountNews(amountNews + 2);
		}
	}

	const addNews = (news) => {
		changeListItem([news, ...listItem]);
	}

	const updateNews = (news) => {
		changeListItem(listItem.map(elem => elem.id === news.id ? news : elem ));
	};

	const removeNews = (newsId) => {
		changeListItem(listItem.filter(item => item.id !== newsId));
	};

	const arrayNewsForShow = () => {
		return listItem.filter((item, index) => {
					if(index < amountNews){
						return item;
					}
				})
	}

	useEffect(() => {
			localStorage.setItem('Новости', JSON.stringify(listItem));
	}, [listItem]);

	return (
		<>
			<BlockAddNews
				addNews={addNews}
			/>
			<NewsList
				listNews={arrayNewsForShow()}
				removeFromProps={removeNews}
				updateFromProps={updateNews}
			/>
			<Button
                theme="more"
                handleClick={moreNews}
            >
                {amountNews < listItem.length ? "Показать еще" : "Новостей больше нет"}
            </Button>
        </>
	);														
}

export default NewsListBlock;