import React, { useState } from 'react';

import Button from '../Button';
import Icon from '../Icon';
import Form from '../Form';

import styles from './style.css';

const Item = ({ item, removeFromList, updateFromList }) => {
    const [isShowForm, handleShowForm] = useState(false);

    const showHideForm = () => {
        handleShowForm(!isShowForm);
    }

    return (
        <li>
            {
                isShowForm ? (
                    <Form
                        item={item}
                        addNews={updateFromList}
                        hideForm={showHideForm}
                        contentForButton="Редактировать"
                    />
                ) : null
            }
            <article className={styles.news}>
                <header>
                    <h1>{item.title}</h1>
                    <div>
                        <Button
                            theme="small"
                            handleClick={showHideForm}
                        >
                            <Icon name="edit" />
                        </Button>
                        <Button
                            theme="small"
                            handleClick={() => removeFromList(item.id)}
                        >
                            <Icon name="delete" />
                        </Button>
                    </div>
                </header>
                <p>{item.text}</p>
            </article>
        </li>
    );
}

export default Item;