import React from 'react';

import Item from '../Item'

import styles from './style.css';

const NewsList = ({ listNews, removeFromProps, updateFromProps }) => {
	return (
		<ul className={styles.newsList}>
			{
				listNews.map((item) => {
					return <Item
						removeFromList={removeFromProps}
				  		updateFromList={updateFromProps}
						key={item.id}
						item={item}
					/>
				})
			}
		</ul>
	);
}

export default NewsList;