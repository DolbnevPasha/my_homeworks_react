import React, { useState } from 'react';

import Button from '../Button';

import styles from './style.css';

const Form = ({item = {}, addNews, hideForm, contentForButton}) => {
    const [title, setTitle] = useState(item.title || '');
    const [text, setText] = useState(item.text || '');

    const handleChange = (e) => {                          
        const { name, value } = e.target;   
        
        switch (name) {
            case 'title':
                setTitle(value);
                break;
            case 'text':
                setText(value);
                break;
            default:
                break;
        } 
    }
    
    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {                           
            id: item.id || Date.now(),     
            title: title,                        
            text: text,                          
        };

        addNews(data);
        hideForm();
        setTitle('');
        setText('');
    }

    return (
        <div
            className={styles.wrap}
        >
            <form
                className={styles.form}
                onSubmit={handleSubmit}
            >
                <label htmlFor="title">Заголовок</label>
                <input
                    type="text"
                    id="title"
                    name="title"
                    value={title}
                    onChange={handleChange}
                />

                <label htmlFor="text">Текст</label>
                <textarea
                    name="text"
                    id="text"
                    value={text}
                    onChange={handleChange}
                />
                <Button
                    theme="post"
                >
                    {contentForButton}
                </Button>
            </form>
        </div>
    );
}

export default Form;