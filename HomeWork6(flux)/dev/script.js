import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import NewsListBlock from './components/NewsListBlock';

import styles from './style.css';

class App extends Component {
	render() {
		return (	
			<div className={styles.box}>
					<NewsListBlock />
			</div>
		);
	}
}

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);