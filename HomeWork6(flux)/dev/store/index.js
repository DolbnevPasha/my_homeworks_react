import EventEmitter from 'events';

let news = JSON.parse(localStorage.getItem('Новости')) || [];

const EVENT = 'news';

const store = Object.assign({}, EventEmitter.prototype, {
	getStore() {
		return news;
	},
	addEventListener(cb) {
		this.addListener(EVENT, cb);
	},
	removeEventListener(cb) {
		this.removeListener(EVENT, cb);
	},
	emitStore() {
		this.emit(EVENT);
		localStorage.setItem('Новости', JSON.stringify(news));
	},
	createNews(newNews) {
		news = [newNews, ...news];
	},
	updateNews(updateNews) {
		news = news.map(item => item.id === updateNews.id ? updateNews : item);
	},
	removeNews(id) {
		news = news.filter(item => item.id !== id);
	}
});

export default store;