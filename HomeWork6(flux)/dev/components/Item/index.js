import React, { Component } from 'react';

import Button from '../Button';
import Icon from '../Icon';
import Form from '../Form';

import styles from './style.css';

class Item extends Component {
    constructor(props) {
        super();

        this.state = {
            isShowForm: false,
            item: props.item,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        const { isShowForm } = this.state;
        this.setState({
            isShowForm: !isShowForm,
        });
    }

    render() {
        const { item } = this.props;
        const { isShowForm } = this.state;
        return (
            <li>
                {
                    isShowForm ? (
                        <Form
                            type="update"
                            item={item}
                            hideForm={this.handleChange}
                            contentForButton="Редактировать"
                        />
                    ) : null
                }
                <article className={styles.news}>
                    <header>
                        <h1>{item.title}</h1>
                        <div>
                            <Button
                                type="editNews"
                                theme="small"
                                handleClick={this.handleChange}
                            >
                                <Icon name="edit" />
                            </Button>
                            <Button
                                type="deleteNews"
                                theme="small"
                                itemId={item.id}
                            >
                                <Icon name="delete" />
                            </Button>
                        </div>
                    </header>
                    <p>{item.text}</p>
                </article>
            </li>
        );
    }
}

export default Item;