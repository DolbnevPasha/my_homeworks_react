import React, { Component } from 'react';

import { removeNews } from '../../actions';

import styles from './style.css';

class Button extends Component {
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.getClassName = this.getClassName.bind(this);
    }

    handleChange(){
        const {type, handleClick, itemId} = this.props;
        switch (type) {
            case 'moreNews':
            case 'addNews':             
            case 'editNews':
                handleClick();
                break;                
            case 'deleteNews':
                removeNews(itemId);
                break;  
            case 'buttonForm':   
                break;               
            default:
                break;
        }
    }

    getClassName(){
        const { theme } = this.props;
        switch (theme) {
            case 'more':
                return styles.more;
            case 'add':
                return styles.add;               
            case 'post':
                return styles.post;                
            case 'small':
                return styles.small;                
            default:
                break;
        }
    }

    render() {
        const { children } = this.props;
        return (
            <button
                className={this.getClassName()}
                onClick={this.handleChange}
            >
                {children}
            </button>
        );
    }
}

export default Button;