import React, { Component } from 'react';

import store from '../../store';

import NewsList from '../NewsList';
import Button from '../Button';
import BlockAddNews from '../BlockAddNews';

class NewsListBlock extends Component {
	constructor(){
		super();

		this.state = {
			amountNews: 2,
			data: store.getStore()
		};

		this.changeDataFromStore = this.changeDataFromStore.bind(this);
		this.moreNews = this.moreNews.bind(this);
        this.arrayNewsForShow = this.arrayNewsForShow.bind(this);
	}

	componentDidMount() {
		store.addEventListener(this.changeDataFromStore);
	}

	componentWillUnmount() {
		store.removeEventListener(this.changeDataFromStore);
	}

	changeDataFromStore() {
		this.setState({
			data: store.getStore(),
		});
	}

	moreNews(){
		const { amountNews, data } = this.state;

		if(amountNews < data.length){
			this.setState({
				amountNews: amountNews+2,
			});
		}
	}
    
    arrayNewsForShow(){
        const { amountNews, data } = this.state;
		return data.filter((item, index) => {
					if(index < amountNews){
						return item;
					}
		        })
	}

	render() {	
		const { amountNews, data } = this.state;															

		return (
			<>
				<BlockAddNews/>
				<NewsList
					listNews={this.arrayNewsForShow()}
                />
				<Button
					type="moreNews"
					theme="more"
					handleClick={this.moreNews}
				>
					{amountNews < data.length ? "Показать еще" : "Новостей больше нет"}
				</Button>
            </>
		);
	}														
}

export default NewsListBlock;