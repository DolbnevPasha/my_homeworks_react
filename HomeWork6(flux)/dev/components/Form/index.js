import React, { Component } from 'react';

import { updateNews, createNews } from '../../actions';

import Button from '../Button';

import styles from './style.css';

class Form extends Component {
    constructor(props){
        super();

        this.state = {
            title: props.item ? props.item.title : '',
            text: props.item ? props.item.text : '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clearForm = this.clearForm.bind(this);
    }

    handleChange(e) {                          
        const { name, value } = e.target;      

        this.setState({                        
            [name]: value,                     
        });
    }
    
    handleSubmit(e) {
        e.preventDefault();

        const { title, text } = this.state;
        const { type, item} = this.props;

        const data = {                           
            id: item ? item.id : Date.now(),     
            title: title,                        
            text: text,                          
        };

        switch (type) {
            case 'create':
                createNews(data);
                break;
            case 'update':
                updateNews(data);
                break;
            default:
                break;
        }
        
        this.props.hideForm();
        this.clearForm();
    }

    clearForm() {                          
        this.setState({
            title: '',
            text: '',
        });
    }

    render() {
        const { title, text } = this.state;
        
        return (
            <div
                className={styles.wrap}
            >
                <form
                    className={styles.form}
                    onSubmit={this.handleSubmit}
                >
                    <label htmlFor="title">Заголовок</label>
                    <input
                        type="text"
                        id="title"
                        name="title"
                        value={title}
                        onChange={this.handleChange}
                    />

                    <label htmlFor="text">Текст</label>
                    <textarea
                        name="text"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                    />
                    <Button
                        type="buttonForm"
                        theme="post"
                    >
                        {this.props.contentForButton}
                    </Button>
                </form>
            </div>
        );
    }
}

export default Form;