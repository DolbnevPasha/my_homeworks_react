import dispatcher from '../dispatcher';

export const createNews = (news) => {
	dispatcher.dispatch({
		type: 'CREATE_NEWS',
		payload: news,
	});
};

export const updateNews = (news) => {
	dispatcher.dispatch({
		type: 'UPDATE_NEWS',
		payload: news,
	});
};

export const removeNews = (id) => {
	dispatcher.dispatch({
		type: 'REMOVE_NEWS',
		payload: id,
	});
};