const defaultStore = JSON.parse(localStorage.getItem('Новости')) || [];

const reducer = (store=defaultStore, { type, payload }) => {
	let news = store;
	switch(type) {
		case 'CREATE_NEWS': {
			news = [payload, ...store];
			localStorage.setItem('Новости', JSON.stringify(news));
			return news;
		}
		case 'UPDATE_NEWS': {
			news = news.map(item => item.id === payload.id ? payload : item);
			localStorage.setItem('Новости', JSON.stringify(news));
			return news;
		}
		case 'REMOVE_NEWS': {
			news = news.filter(item => item.id !== payload);
			localStorage.setItem('Новости', JSON.stringify(news));
			return news;
		}
		default: {
			return news;
		}
	}
};

export default reducer;