import React, { Component } from 'react';

import Button from '../Button';
import Form from '../Form';

import styles from './style.css';

class BlockAddNews extends Component {
    constructor(){
        super();

        this.state = {
            isShowForm: false,
        };

        this.showHideForm = this.showHideForm.bind(this);
    }

    showHideForm() {
		const { isShowForm } = this.state;
		this.setState({
			isShowForm: !isShowForm,
		});
    }

    render() {
        const { isShowForm } = this.state;

        return (
            <div
                className={styles.block}
            >
                <Button
                    type="addNews"
                    theme="add"
                    handleClick={this.showHideForm}
                >
                    Добавить новость
                </Button>
                {
                    isShowForm ? (
                        <Form
                            type="create"
                            hideForm={this.showHideForm}
                            contentForButton="Опубликовать"
                        />
                    ) : null
                }
            </div>
        );
    }
}

export default BlockAddNews;