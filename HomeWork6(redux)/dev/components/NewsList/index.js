import React, { Component } from 'react';

import Item from '../Item';

import styles from './style.css';

class NewsList extends Component {
	render() {
		const { listNews } = this.props;
		return (
			<ul className={styles.newsList}>
				{
					listNews.map((item) => {
						return <Item
							key={item.id}
							item={item}
						/>
					})
				}
			</ul>
		);
	}
}

export default NewsList;