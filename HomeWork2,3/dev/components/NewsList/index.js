import React, { Component } from 'react';

import Item from '../Item';

class NewsList extends Component {

	render() {
		const { listNews, removeFromProps, updateFromProps } = this.props;
		return (
			<ul className="newsList">
				{
					listNews.map((item) => {
						return <Item
							removeFromList={removeFromProps}
							updateFromList={updateFromProps}
							key={item.id}
							item={item}
						/>
					})
				}
			</ul>
		);
	}
}

export default NewsList;