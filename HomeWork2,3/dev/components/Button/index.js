import React, { Component } from 'react';

class Button extends Component {

    render() {
        const {theme, handleClick, children} = this.props;
        return (
            <button 
                className={theme}
                onClick={handleClick}
			>
				{children}
			</button>
        );
    }
}

export default Button;