import React, { Component } from 'react';

import Button from '../Button';
import Form from '../Form';

class BlockAddNews extends Component {
    constructor(){
        super();

        this.state = {
            isShowForm: false,
        };

        this.showHideForm = this.showHideForm.bind(this);
    }

    showHideForm() {
		const { isShowForm } = this.state;
		this.setState({
			isShowForm: !isShowForm,
		});
    }

    render() {
        const { isShowForm } = this.state;

        return (
            <div
                className="block__addNews"
            >
                <Button
                    theme="add__news"
                    handleClick={this.showHideForm}
                >
                    Добавить новость
                </Button>
                {
                    isShowForm ? (
                        <Form
                            addNews={this.props.addNews}
                            hideForm={this.showHideForm}
                            contentForButton="Опубликовать"
                        />
                    ) : null
                }
            </div>
        );
    }
}

export default BlockAddNews;