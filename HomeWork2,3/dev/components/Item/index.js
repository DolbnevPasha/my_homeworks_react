import React, { Component } from 'react';

import Button from '../Button';
import Icon from '../Icon';
import Form from '../Form';

class Item extends Component {
    constructor(props) {
        super();

        this.state = {
            isShowForm: false,
            item: props.item,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        const { isShowForm } = this.state;
        this.setState({
            isShowForm: !isShowForm,
        });
    }

    render() {
        const { item, removeFromList, updateFromList } = this.props;
        const { isShowForm } = this.state;

        return (
            <li>
                {
                    isShowForm ? (
                        <Form
                            item={item}
                            addNews={updateFromList}
                            hideForm={this.handleChange}
                            contentForButton="Редактировать"
                        />
                    ) : null
                }
                <article className="news">
                    <header>
                        <h1>{item.title}</h1>
                        <div>
                            <Button
                                theme="button--small"
                                handleClick={this.handleChange}
                            >
                                <Icon name="edit" />
                            </Button>
                            <Button
                                theme="button--small"
                                handleClick={() => removeFromList(item.id)}
                            >
                                <Icon name="delete" />
                            </Button>
                        </div>
                    </header>
                    <p>{item.text}</p>
                </article>
            </li>
        );
    }
}

export default Item;