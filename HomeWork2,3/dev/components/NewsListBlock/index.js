import React, { Component } from 'react';

import NewsList from '../NewsList';
import Button from '../Button';
import BlockAddNews from '../BlockAddNews';

class NewsListBlock extends Component {
	constructor(){
		super();

		this.state = {
			amountNews: 2,
			listItem: JSON.parse(localStorage.getItem('Новости')) ? JSON.parse(localStorage.getItem('Новости')) : [],
		};

		this.moreNews = this.moreNews.bind(this);
		this.addNews = this.addNews.bind(this);
		this.updateNews = this.updateNews.bind(this);
		this.removeNews = this.removeNews.bind(this);
        this.arrayNewsForShow = this.arrayNewsForShow.bind(this);
	}

	moreNews(){
		const { amountNews, listItem } = this.state;

		if(amountNews < listItem.length){
			this.setState({
				amountNews: amountNews+2,
			});
		}
	}

	addNews(news){
		const { listItem } = this.state;
		this.setState({
			listItem: [news, ...listItem],
		}, () => {
			localStorage.setItem('Новости', JSON.stringify(this.state.listItem));
		});
	}

	updateNews(news) {
		const { listItem } = this.state;
		this.setState({
			listItem: listItem.map(elem => (
				elem.id === news.id ? news : elem
			))
		}, () => {
			localStorage.setItem('Новости', JSON.stringify(this.state.listItem));
		});
	}
	
	removeNews(newsId){
		const{ listItem } = this.state;
		this.setState({
			listItem: listItem.filter(item => item.id !== newsId)
		}, () => {
			localStorage.setItem('Новости', JSON.stringify(this.state.listItem));
		});
	}
    
    arrayNewsForShow(){
        const { amountNews, listItem } = this.state;
		return listItem.filter((item, index) => {
					if(index < amountNews){
						return item;
					}
		        })
	}

	render() {	
		const { amountNews, listItem } = this.state;															

		return (
			<>
				<BlockAddNews
					addNews={this.addNews}
				/>
				<NewsList
					listNews={this.arrayNewsForShow()}
					removeFromProps={this.removeNews}
					updateFromProps={this.updateNews}
                />
				<Button
					theme="more__news"
					handleClick={this.moreNews}
				>
					{amountNews < listItem.length ? "Показать еще" : "Новостей больше нет"}
				</Button>
            </>
		);
	}														
}

export default NewsListBlock;