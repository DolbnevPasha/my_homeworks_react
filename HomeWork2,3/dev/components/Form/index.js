import React, { Component } from 'react';

import Button from '../Button';

class Form extends Component {
    constructor(props){
        super();

        this.state = {
            title: props.item ? props.item.title : '',
            text: props.item ? props.item.text : '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clearForm = this.clearForm.bind(this);
    }

    handleChange(e) {                          
        const { name, value } = e.target;      

        this.setState({                        
            [name]: value,                     
        });
    }
    
    handleSubmit(e) {
        e.preventDefault();

        const { title, text } = this.state;
        const { item } = this.props;

        const data = {                           
            id: item ? item.id : Date.now(),     
            title: title,                        
            text: text,                          
        };

        this.props.addNews(data);
        this.props.hideForm();
        this.clearForm();
    }

    clearForm() {                          
        this.setState({
            title: '',
            text: '',
        });
    }

    render() {
        const { title, text } = this.state;
        
        return (
            <div
                className="form__wrap"
            >
                <form
                    className="form"
                    onSubmit={this.handleSubmit}
                >
                    <label htmlFor="title">Заголовок</label>
                    <input
                        type="text"
                        id="title"
                        name="title"
                        value={title}
                        onChange={this.handleChange}
                    />

                    <label htmlFor="text">Текст</label>
                    <textarea
                        name="text"
                        id="text"
                        value={text}
                        onChange={this.handleChange}
                    />
                    <Button
                        theme="button__post"
                    >
                        {this.props.contentForButton}
                    </Button>
                </form>
            </div>
        );
    }
}

export default Form;