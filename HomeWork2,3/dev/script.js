import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import NewsListBlock from './components/NewsListBlock'

class App extends Component {
	render() {
		return (	
			<div className="box">
					<NewsListBlock />
			</div>
		);
	}
}

ReactDOM.render(
	<App/>,
	document.getElementById('app')
);