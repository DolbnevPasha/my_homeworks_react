import React, { Component } from 'react'; 

import Comment1 from '../Comment1';
import Comment2 from '../Comment2';

class CommentsList extends Component {                    
	render() {									 
		return (								 
			<ul class="list">								 
				<Comment1 />
                <Comment2 />
            </ul>
		);
	}
}

export default CommentsList;