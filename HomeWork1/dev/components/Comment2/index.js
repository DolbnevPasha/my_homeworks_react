import React, { Component } from 'react';

class Comment2 extends Component{
    render(){
        return (
            <li class="item">
                <header class="item__head">
                    <h3 class="item__title">Юрий</h3>
                    <div class="item__action">
                        <button class="button button--small">edit</button>
                        <button class="button button--small">delete</button>
                    </div>
                </header>
                <p>Всем привет</p>
            </li>
        );
    }
}

export default Comment2;